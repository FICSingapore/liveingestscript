import java.io.*
import java.util.*;
import java.util.List
import java.util.LinkedHashMap;
import java.util.Map;
import com.theplatform.ingest.adapter.api.AdapterResultItem
import com.theplatform.ingest.data.objects.IngestMedia
import com.theplatform.ingest.data.objects.IngestMediaFile
import com.theplatform.media.api.data.objects.Expression
import java.util.concurrent.BlockingQueue
import com.theplatform.media.api.data.objects.Media
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Sheet
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import java.text.SimpleDateFormat;
import com.theplatform.data.api.objects.type.CategoryInfo
import com.theplatform.media.api.data.objects.TransferInfo
import com.theplatform.ingest.data.objects.IngestOptions
import com.theplatform.ingest.data.objects.IngestMethod
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.Hashtable;
import com.theplatform.groovy.helper.MediaHelper
import com.theplatform.media.api.data.objects.ContentType
import com.theplatform.media.api.data.objects.Format
import com.theplatform.media.api.data.objects.FormatClient
import com.theplatform.ingest.data.objects.WorkflowOption;





TextFileReader parser = new TextFileReader(resultQueue, metadata,mediaHelper,formatClient)
parser.parse()


// read every row of text file and create or update media based on the media ID
public class TextFileReader implements LineParsedListener {

	InputStream metadata
	BlockingQueue<AdapterResultItem> resultQueue
	List<IngestMedia> medias = new ArrayList<IngestMedia>()
	List<IngestMediaFile> ingestMediaFiles = new ArrayList<IngestMediaFile>()
	MediaHelper mediaHelper;
	Boolean isObjectAddedToTheQue = false;
	ArrayList temporarMediaArray = new ArrayList();
	Map mediaMap = new HashMap();
	FormatClient formatClient;

	def accountID = "http://access.auth.theplatform.com/data/Account/2337729561"
	def imageThumb = "http://devapps.foxsportsasia.com/img-fsp/FICLIVE.png"



	Dictionary channelDictionary = new Hashtable();
	Dictionary categoriesDictionay = new Hashtable();
	HashMap<String, List<String>> countryMap = new HashMap<String, List<String>>();
	Dictionary showsDictionary = new Hashtable();
	Dictionary vodDictionary = new Hashtable();

	// Array contains show names to create VOD
	def vodArray = ["Fox Sports Central", "Fox Sports FC", "Friday Night Football"];

	public TextFileReader(BlockingQueue<AdapterResultItem> resultQueue, InputStream metadata,MediaHelper mediaHelper, FormatClient formatClient) {
		this.metadata = metadata
		this.resultQueue = resultQueue
		this.mediaHelper = mediaHelper
		this.formatClient = formatClient

		// Map Hong Kong categories to Our categories
		categoriesDictionay.put("Auto Racing/FIA Formula One World Championship","Auto Racing/FIA Formula One World C'ship");
		categoriesDictionay.put("Motor Racing/Motorcycle","Motor Cycle Racing/MotoGP World Championship");
		categoriesDictionay.put("Auto Racing/Formula E Championship","Auto Racing/Formula E");
		categoriesDictionay.put("Baseball/Major League Baseball","Baseball/Major League Baseball (Mlb)");
		categoriesDictionay.put("Motor Racing/Motocross","Motor Cycle Racing/Motocross Programming");
		categoriesDictionay.put("International Sports News/International Sports Review","International Sports News");
		categoriesDictionay.put("Golf/Masters, The","Golf/Masters");
		categoriesDictionay.put("Golf/Masters,The","Golf/Masters");
		categoriesDictionay.put("Golf/Open Championship, The","Golf/Open Championship");
		categoriesDictionay.put("Motor Racing/Motor Sports","Motor Cycle Racing/SBK World Superbikes C'ship");
		categoriesDictionay.put("Awards/Sports","Awards");
		categoriesDictionay.put("Tennis/Roland Garros","Tennis/French Open");
		categoriesDictionay.put("Mixed Martial Arts/Mixed Martial Arts","Martial Arts/Martial Arts");
		categoriesDictionay.put("Tennis/Wimbledon","Tennis/Wimbledon Lawn Tennis C'ship");

		// Define VOD Dictioany which we will be creating VOD dynamically
		List<String> vodList = new ArrayList<String>();
		vodList.add("Fox Sports Central");
		vodList.add(1);
		vodDictionary.put("Fox Sports Central",vodList);

		vodList = new ArrayList<String>();
		vodList.add("FOX Sports FC");
		vodList.add(7);
		vodDictionary.put("FOX Sports FC",vodList);

		vodList = new ArrayList<String>();
		vodList.add("Friday Night Football");
		vodList.add(7);
		vodDictionary.put("Friday Night Football",vodList);

		// Define Shows and Default image for shows
		List<String> showList = new ArrayList<String>()
		showList.add("FOX Sports Fit - Sister Hiit");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Sister_HIIT.jpg");
		showsDictionary.put("FOX Sports Fit - Sister Hiit",showList)

		showList = new ArrayList<String>()
		showList.add("FOX Sports Fit - 101 Exercises To Do Before You Die");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/101.jpg");
		showsDictionary.put("FOX Sports Fit - 101 Exercises To Do Before You Die",showList)

		showList = new ArrayList<String>()
		showList.add("Sport Confidential");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Sport_Confidential.jpg");
		showsDictionary.put("Sport Confidential",showList)

		showList = new ArrayList<String>()
		showList.add("FOX SPORTS Fit - Reebok Crossfit");
		showList.add("http://images.foxplayasia.com/images/Fox_Sports_Asia_Prod_CP/754/823/crossfit.png");
		showsDictionary.put("FOX SPORTS Fit - Reebok Crossfit",showList)

		showList = new ArrayList<String>()
		showList.add("Badminton Unlimited");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Badminton_Unlimited.jpg");
		showsDictionary.put("Badminton Unlimited",showList)

		showList = new ArrayList<String>()
		showList.add("National Icons");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/National_Icons.jpg");
		showsDictionary.put("National Icons",showList)

		showList = new ArrayList<String>()
		showList.add("FOX Sports Fit - Fit For Fashion");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Fit_For_Fashion.jpg");
		showsDictionary.put("FOX Sports Fit - Fit For Fashion",showList)

		showList = new ArrayList<String>()
		showList.add("FOX Sports Fit - STAR Block Workout");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Star_Block_Workout.jpg");
		showsDictionary.put("FOX Sports Fit - STAR Block Workout",showList)

		showList = new ArrayList<String>()
		showList.add("FOX Sports Fit - Kirei Yoga");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Kirei_Yoga.jpg");
		showsDictionary.put("FOX Sports Fit - Kirei Yoga",showList)

		showList = new ArrayList<String>()
		showList.add("Fox Sports Central");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/FS_Central.jpg");
		showsDictionary.put("Fox Sports Central",showList)

		showList = new ArrayList<String>()
		showList.add("KIA World Extreme Games");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/World_Extreme_Games.jpg");
		showsDictionary.put("KIA World Extreme Games",showList)

		showList = new ArrayList<String>()
		showList.add("Sports Unlimited");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Sports_Unlimited.jpg");
		showsDictionary.put("Sports Unlimited",showList)

		showList = new ArrayList<String>()
		showList.add("Watersports World");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Water_Sports_World.jpg");
		showsDictionary.put("Watersports World",showList)

		showList = new ArrayList<String>()
		showList.add("Planet Speed");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Planet_Speed.jpg");
		showsDictionary.put("Planet Speed",showList)

		showList = new ArrayList<String>()
		showList.add("Lumberjacks Season");
		showList.add("http://images.foxplayasia.com/images/Fox_Sports_Asia_Prod_CP/626/95/lumberjack1.png");
		showsDictionary.put("Lumberjacks Season",showList)

		showList = new ArrayList<String>()
		showList.add("Inside Grand Prix");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Inside_Grand_Prix.jpg");
		showsDictionary.put("Inside Grand Prix",showList)

		showList = new ArrayList<String>()
		showList.add("FIBA World Basketball Weekly");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/FIBA.jpg");
		showsDictionary.put("FIBA World Basketball Weekly",showList)

		showList = new ArrayList<String>()
		showList.add("Liga Bbva 2014/15 Weekly");
		showList.add("http://images.foxplayasia.com/images/Fox_Sports_Asia_Prod_CP/176/175/La Liga Weekly Review.jpg");
		showsDictionary.put("Liga Bbva 2014/15 Weekly",showList)

		showList = new ArrayList<String>()
		showList.add("UFC UNLEASHED");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/UFC.jpg");
		showsDictionary.put("UFC UNLEASHED",showList);

		showList = new ArrayList<String>()
		showList.add("Red Bull Air Race");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Red_Bull_Air_Race3.jpg");
		showsDictionary.put("Red Bull Air Race",showList);

		// Fox Sports FC
		showList = new ArrayList<String>()
		showList.add("Fox Sports FC");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/FS_FC.jpg");
		showsDictionary.put("Fox Sports FC",showList);

		// Friday Night Football
		showList = new ArrayList<String>()
		showList.add("Friday Night Football");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Friday_Night_Football_960_540.jpg");
		showsDictionary.put("Friday Night Football",showList);

		// Goals show
		showList = new ArrayList<String>()
		showList.add("Goals");
		showList.add("http://devapps.foxsportsasia.com/img-fsp/Goals_960x540.jpg");
		showsDictionary.put("Goals",showList);

		// Set country code and encoder info for each channels

		// --------- HK Channels - Fox Sports NPL & Fox Sports 2 NPL & Fox Sports 3 ---------------
		List<String> values = new ArrayList<String>();
		values.add("HK");
		countryMap.put("FSHK", values);
		ArrayList<String> linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS SEA NPL");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs_hk_1@312613/master.m3u8?hdnea=st={date:-120:true}~exp={date:120:true}~acl=/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS SEA NPL");
		linktofile.add("FoxSportsHongKongAndThaiLand");
		channelDictionary.put("FSHK", linktofile);

		// ------------------- Consolidating all FS2 Channels into One ( All FS2 Except FS2Malaysia are changed to FS2 HK) --------------------------------
		values = new ArrayList<String>();
		values.add("HK");
		values.add("TH");
		values.add("ID");
		values.add("PH");
		values.add("SG");
		countryMap.put("SEN1", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS 2 NPL");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs2_hk_1@312614/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS 2 NPL");
		linktofile.add("FoxSports2HK");
		channelDictionary.put("SEN1", linktofile);

		values = new ArrayList<String>();
		values.add("HK");
		values.add("ID");
		values.add("PH");
		values.add("SG");
		values.add("TH");
		values.add("MY");
		countryMap.put("ESEA", values);
		linktofile = new ArrayList<String>();
		linktofile.add("Fox SPORTS 3");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs3_hk_1@312615/master.m3u8?hdnea=st={date:-120:true}~exp={date:120:true}~acl=/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("Fox SPORTS 3");
		linktofile.add("FoxSports3");
		channelDictionary.put("ESEA", linktofile);

		// --------- HK Channels - Fox Sports NPL & Fox Sports 2 NPL & Fox Sports 3 ---------------

		// --------- TH Channels - Fox Sports Thailand & Fox Sports 2 NPL & Fox Sports 3 ---------------
		values = new ArrayList<String>();
		values.add("TH");
		countryMap.put("FSTH", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS Thailand");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs_th_1@312616/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS Thailand");
		linktofile.add("FoxSportsThailand");
		channelDictionary.put("FSTH", linktofile);

		//-------------------------------------------------------------------------------

		// --------- Indonesia Channel - Fox Sports Indonesia -------------------------------------

		values = new ArrayList<String>();
		values.add("ID");
		countryMap.put("EID1", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS Indonesia");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs_id_1@312609/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS Indonesia");
		linktofile.add("FoxSportsIndonesia");
		channelDictionary.put("EID1", linktofile);
		// --------- Indonesia Channel - Fox Sports Indonesia -------------------------------------


		// --------- Singapore Channel - Fox Sports S.E.A----------------------

		values = new ArrayList<String>();
		values.add("SG");
		countryMap.put("ESG1", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS S.E.A");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs_sg_1@312610/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS S.E.A");
		linktofile.add("FoxSportsSingapore");
		channelDictionary.put("ESG1", linktofile);

		// --------- Singapore Channel - Fox Sports S.E.A----------------------

		// --------- Malaysia Channel - Fox Sports Malaysia & Fox Sports 2 Malaysia -----------------

		values = new ArrayList<String>();
		values.add("MY");
		countryMap.put("EML1", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS Malaysia");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs_my_1@312606/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS Malaysia");
		linktofile.add("FoxSportsMalaysia");
		channelDictionary.put("EML1", linktofile);

		values = new ArrayList<String>();
		values.add("MY");
		countryMap.put("F2M1", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS 2 Malaysia");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs2_my_1@312607/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS 2 Malaysia");
		linktofile.add("FoxSports2Malaysia");
		channelDictionary.put("F2M1", linktofile);

		// --------- Malaysia Channel - Fox Sports Malaysia & Fox Sports 2 Malaysia -----------------

		// --------- Philippine Channel -  Fox Sports Philippine ------------------------------------

		values = new ArrayList<String>();
		values.add("PH");
		countryMap.put("EPH1", values);
		linktofile = new ArrayList<String>();
		linktofile.add("FOX SPORTS Philippines");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs_ph_1@312608/master.m3u8?hdnts=st={date:-120:true}~exp={date:120:true}~acl=/i/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("FOX SPORTS Philippines");
		linktofile.add("FoxSportsPhilipines");
		channelDictionary.put("EPH1", linktofile);

		// --------- Philippine Channel -  Fox Sports Philippine ------------------------------------

		values = new ArrayList<String>();
		values.add("HK");
		values.add("ID");
		values.add("PH");
		values.add("SG");
		values.add("TH");
		values.add("MY");
		countryMap.put("FM31", values);
		linktofile = new ArrayList<String>();
		linktofile.add("Fox SPORTS 3");
		linktofile.add("http://fspsecurehls-lh.akamaihd.net/i/fs3_hk_1@312615/master.m3u8?hdnea=st={date:-120:true}~exp={date:120:true}~acl=/*~hmac={token:AkamaiEdgeAuth:68CCD77E7B0277C257F74A2ADD3BE796}&dw=30");
		linktofile.add("Fox SPORTS 3");
		linktofile.add("FoxSports3");
		channelDictionary.put("FM31", linktofile);




	}

	public void parse() {
		TextParser parser = new TextParser(metadata,this)
		parser.process(channelDictionary)
	}
	public String getDateTime(String dateString , String timeString){

		String year = dateString.substring(0, Math.min(dateString.length(), 4));
		String month = dateString.substring(4, Math.min(dateString.length(),6));
		String currentDate = dateString.substring(6, Math.min(dateString.length(),8));

		return currentDate+"/"+month+"/"+    year+" "+timeString;
	}


	// Remove # from Media Metadata Fields
	public String removeHashString(String stringToCheckForHash){
		int poundIngex = stringToCheckForHash.indexOf("#");
		if (poundIngex != -1)
		{
			return stringToCheckForHash.substring(0, poundIngex);
		}

		return stringToCheckForHash;
	}


	/*
	 *  Update Media Custom values, thumbnails & m3u8 files
	 */

	public void setMediaValues(IngestMedia media,String[] row,Date availableDate, Date expirationDate,boolean isShow,linkIngestMediaFiles,ingestMediaThumbnailFiles,boolean update,boolean isVod){

		String keywords = row[11].replaceAll("\"", "")+","+ row[10].replaceAll("\"", "");
		keywords = keywords.startsWith(",") ? keywords.substring(1) : keywords;
		media.keywords = keywords.endsWith(",") ? keywords.substring(0,keywords.length()-1) : keywords;

		String airCategory = row[13];

		media.setCustomValue(accountID, "Venue", removeHashString(row[10].replaceAll("\"", "")));
		media.setCustomValue(accountID, "Keywords", removeHashString(media.keywords));
		media.setCustomValue(accountID, "specialAiringCategory",(row[13]=="L" && !isVod)?"LIVE (OD)":"VOD");
		media.setCustomValue(accountID, "sourceChannel", channelDictionary.get(row[0].replaceAll("\"", "")).get(0));
		media.setCustomValue(accountID, "seriesName", removeHashString(row[14]));
		media.setCustomValue(accountID, "episodeNo", (row[13]=="L" && !isVod)?"":removeHashString(row[22]));

		media.setCustomValue(null, "isLive", (row[13]=="L" && !isVod)?true:false);
		media.setCustomValue(null, "isFree", false);
		media.setCustomValue(null, "isShow", isShow);
		media.setCustomValue(null, "isChannel", false);
		media.setCustomValue(null, "isExtraCam", false);
		def competitorValues = null;

		boolean isStringContaingVSDot =  row[11].replaceAll("\"", "").contains('vs.');

		if(isStringContaingVSDot){
			competitorValues = row[11].replaceAll("\"", "").split(' vs. ');
		} else {
			competitorValues = row[11].replaceAll("\"", "").split(' vs ');
		}


		media.setCustomValue(accountID, "CompetitorA", removeHashString(competitorValues[0]));
		if(competitorValues.size()>1){
			media.setCustomValue(accountID, "CompetitorB", removeHashString(competitorValues[1]));
		}



		// set workflow labels
		def workflowLabel = new String[1]
		workflowLabel[0] = channelDictionary.get(row[0].replaceAll("\"", "")).get(2)
		media.setAdminTags(workflowLabel)

		// set categories based on the text file information
		CategoryInfo catInfo = new CategoryInfo()

		String parentCategory = row[1].replaceAll("\"", "");
		String parentCategoryAfterChangingSoccer =  parentCategory.toLowerCase().startsWith("Soccer".toLowerCase())?parentCategory.replaceAll("Soccer","Football"):parentCategory;
		String subCategory = row[2].replaceAll("\"", "");
		String subCategoryAfterChangingSoccer = subCategory.toLowerCase().startsWith("Soccer".toLowerCase())?subCategory.replaceAll("Soccer","Football"):subCategory;
		String categoryName = parentCategoryAfterChangingSoccer +"/"+subCategoryAfterChangingSoccer;
		categoryName = categoryName.replaceAll("None","");
		categoryName = categoryName.startsWith("/") ? categoryName.substring(1) : categoryName;
		String replaceSpecialCharacters = switchCategories(replaceXmlSpecialCharacters(categoryName.endsWith("/") ? categoryName.substring(0,categoryName.length()-1) : categoryName));

		catInfo.name = replaceSpecialCharacters;

		def  catList = []
		catList << catInfo;
		media.categories = catList.toArray(new CategoryInfo[catList.size()])
		
		TransferInfo transferInfo = new TransferInfo()
		IngestOptions ingestOptions = new IngestOptions()

		if(!isVod){
				
			String mediaFileID = row[17].replaceAll("\"", "")+row[0].replaceAll("\"","")
			
					if (update){
			
						IngestMediaFile fileToDelete = new IngestMediaFile()
						fileToDelete.guid = mediaFileID;
						fileToDelete.deleted = true
						linkIngestMediaFiles << fileToDelete
			
					}
			
					IngestMediaFile mediaFileSource = new IngestMediaFile()
					mediaFileSource.url = channelDictionary.get(row[0].replaceAll("\"", "")).get(1)+"&start="+media.availableDate.getTime()/1000;
					mediaFileSource.assetTypes = [channelDictionary.get(row[0].replaceAll("\"", "")).get(3)]
					mediaFileSource.contentType = ContentType.video
					Format format1 = formatClient.getByExtension("m3u")
					mediaFileSource.format = format1
					mediaFileSource.guid = mediaFileID;
					mediaFileSource.deleted = false
					
					transferInfo.supportsStreaming = true
					mediaFileSource.transferInfo = transferInfo
					
					ingestOptions.method = IngestMethod.Link
					mediaFileSource.ingestOptions = ingestOptions
					mediaFileSource.isDefault = true
			
			
			
			
					linkIngestMediaFiles << mediaFileSource
					media.setContent(linkIngestMediaFiles.toArray(new IngestMediaFile[linkIngestMediaFiles.size()]))
			
		}
		




		// Set Mezzanine Image
		if(update){

			IngestMediaFile fileToDelete = new IngestMediaFile()
			fileToDelete.guid = isVod?row[18].replaceAll("\"", "")+"IMG": row[17].replaceAll("\"", "")+"IMG";
			fileToDelete.deleted = true
			ingestMediaThumbnailFiles << fileToDelete

		}

		IngestMediaFile mediaFileThumb = new IngestMediaFile()
		mediaFileThumb.url = imageThumb
		mediaFileThumb.assetTypes = ["Mezzanine Image"]
		transferInfo.supportsStreaming = true
		mediaFileThumb.transferInfo = transferInfo
		ingestOptions.method = IngestMethod.Link
		mediaFileThumb.ingestOptions = ingestOptions
		mediaFileThumb.guid = isVod?row[18].replaceAll("\"", "")+"IMG": row[17].replaceAll("\"", "")+"IMG";
		ingestMediaThumbnailFiles << mediaFileThumb

		// Set media thumbnails & media contents
		media.setThumbnails(ingestMediaThumbnailFiles.toArray(new IngestMediaFile[ingestMediaThumbnailFiles.size()]))
		

	}

	/*
	 * Create the VOD for the ingesting media  
	 */
	public void createVOD(IngestMedia mediaToIngest,List<String> vodDictObj){

		def namespace = "http://access.auth.theplatform.com/data/Account/2337729561"

		//		if(mediaToIngest.title.toLowerCase().startsWith("Fox Sports Central".toLowerCase())){

		IngestMedia vodeMedia = new IngestMedia();
		boolean update =  false;
		if (mediaHelper.mediaExistsByGuid(mediaToIngest.guid+"VOD")){
			update = true;
		}
		vodeMedia.guid = mediaToIngest.guid+"VOD";
		vodeMedia.title = mediaToIngest.title+" - Replay";
		vodeMedia.availableDate = mediaToIngest.expirationDate;

		// add 24 hours to the expirationDate
		Calendar cal = Calendar.getInstance();
		cal.setTime(mediaToIngest.availableDate);
		cal.add(Calendar.DATE, vodDictObj.get(1)); //minus number would decrement the days
		vodeMedia.expirationDate =  cal.getTime();


		List<String> list = new ArrayList<String>();
		list.addAll(mediaToIngest.countries);

		vodeMedia.countries = list.toArray()
		vodeMedia.setCustomValue(namespace, "allowedCountry",mediaToIngest.countries);
		vodeMedia.excludeCountries = false
		vodeMedia.categories = mediaToIngest.categories;
		vodeMedia.setCustomValue(namespace, "specialAiringCategory","VOD");
		vodeMedia.setCustomValue(null, "isLive", false);
		vodeMedia.setCustomValue(null, "isFree", false);
		vodeMedia.setCustomValue(null, "isShow", true);
		vodeMedia.setCustomValue(null, "isChannel", false);
		vodeMedia.setCustomValue(null, "isExtraCam", false);

		def  ingestMediaThumbnailFiles = []
		def  linkIngestMediaFiles = []

		String mediaFileID = mediaToIngest.guid;



		if (update){

			IngestMediaFile fileToDelete = new IngestMediaFile()
			fileToDelete.guid = mediaFileID+"M3UVOD";
			fileToDelete.deleted = true
			linkIngestMediaFiles << fileToDelete

		}

		IngestMediaFile[] imfArr = mediaToIngest.content
		TransferInfo transferInfo = new TransferInfo()
		IngestOptions ingestOptions = new IngestOptions()

		List<IngestMediaFile> mediaFileList = new ArrayList<IngestMediaFile>();
		mediaFileList.addAll(mediaToIngest.content)

		for(IngestMediaFile imf : mediaFileList){
			IngestMediaFile mediaFileSource = new IngestMediaFile()
			mediaFileSource.url = imf.url+"&end="+mediaToIngest.expirationDate.getTime()/1000;
			mediaFileSource.assetTypes = imf.assetTypes;
			mediaFileSource.contentType = ContentType.video
			Format format1 = formatClient.getByExtension("m3u")
			mediaFileSource.format = format1
			mediaFileSource.guid = imf.guid+"M3UVOD";
			mediaFileSource.deleted = false

			transferInfo.supportsStreaming = true
			mediaFileSource.transferInfo = transferInfo

			ingestOptions.method = IngestMethod.Link
			mediaFileSource.ingestOptions = ingestOptions
			mediaFileSource.isDefault = true
			linkIngestMediaFiles << mediaFileSource
		}


		//		for (imf in imfArr)
		//		{
		//			IngestMediaFile mediaFileSource = new IngestMediaFile()
		//			mediaFileSource.url = imf.url+"&end="+mediaToIngest.expirationDate.getTime()/1000;
		//			mediaFileSource.assetTypes = imf.assetTypes;
		//			mediaFileSource.contentType = ContentType.video
		//			Format format1 = formatClient.getByExtension("m3u")
		//			mediaFileSource.format = format1
		//			mediaFileSource.guid = imf.guid+"M3UVOD";
		//			mediaFileSource.deleted = false
		//
		//			transferInfo.supportsStreaming = true
		//			mediaFileSource.transferInfo = transferInfo
		//
		//			ingestOptions.method = IngestMethod.Link
		//			mediaFileSource.ingestOptions = ingestOptions
		//			mediaFileSource.isDefault = true
		//			linkIngestMediaFiles << mediaFileSource
		//		}

		// Set Mezzanine Image
		if(update){

			IngestMediaFile fileToDelete = new IngestMediaFile()
			fileToDelete.guid = mediaFileID+"IMGVOD";
			fileToDelete.deleted = true
			ingestMediaThumbnailFiles << fileToDelete

		}



		IngestMediaFile[] thumbnails = mediaToIngest.getThumbnails()

		for (thumb in thumbnails)
		{
			IngestMediaFile mediaFileThumb = new IngestMediaFile()
			mediaFileThumb.url = thumb.url;
			mediaFileThumb.assetTypes = ["Mezzanine Image"]
			mediaFileThumb.transferInfo = transferInfo
			mediaFileThumb.ingestOptions = ingestOptions
			mediaFileThumb.guid = thumb.guid+"IMGVOD";
			ingestMediaThumbnailFiles << mediaFileThumb


		}


		// Set media thumbnails & media contents
		vodeMedia.setThumbnails(ingestMediaThumbnailFiles.toArray(new IngestMediaFile[ingestMediaThumbnailFiles.size()]))
		vodeMedia.setContent(linkIngestMediaFiles.toArray(new IngestMediaFile[linkIngestMediaFiles.size()]))


		// Automatically Publish the Fox Sports Central media
		setWorkflowOption(vodeMedia);



		// Add the VOD into the results Queue
		AdapterResultItem results = new AdapterResultItem()
		results.setMedia(vodeMedia);
		resultQueue.put(results);

		return;
		//		}

	}

	// Set publishing profile for Auto Publish
	public void setWorkflowOption(IngestMedia mediaToIngest){
		
		WorkflowOption publishWorkFlow = new WorkflowOption()
		// Set the options for the publish action
		publishWorkFlow.setService("sharing")
		publishWorkFlow.setMethod("republish")

		// The profile argument tells the publish service
		// which publish profile to use with this media.
		Map<String, String> argMap = new HashMap<String, String>()
		argMap.put("profileId", "http://data.share.theplatform.com/sharing/data/OutletProfile/19938")

		publishWorkFlow.setArguments(argMap)

		// Assign the workflow request to the media.
		def woArr = new WorkflowOption[1]
		woArr[0] = publishWorkFlow
		mediaToIngest.setWorkflowOptions(woArr);
	}

	public boolean isVODNeededToCreate(String mediaTitle){

		for(String title : vodArray){
			if(mediaTitle.startsWith(title)){
				return true;

			}
		}

		return false;
	}
	/**
	 * Text file processing is finished and add each element in temporary media map to the adapter results queue
	 */
	public void finishedProcessing(){
		// Get a set of the entries
		Set mediaSet = mediaMap.entrySet();
		// Get an iterator
		Iterator i = mediaSet.iterator();

		// Add each media to adapter resputsQues
		while(i.hasNext()) {
			Map.Entry me = (Map.Entry)i.next();

			IngestMedia mediaToIngest = me.getValue()

			List<String> vod = isVodCreationNeeded(mediaToIngest.title,mediaToIngest,accountID);

			if(vod!=null){


				// if Fox Sports FC then change the media url & expiration Date only.
				if(mediaToIngest.title.toLowerCase().startsWith("Fox Sports FC".toLowerCase())){

					mediaToIngest.title = mediaToIngest.title + " - Replay";

					// changed the media expiration time to

					// add 24 hours to the expirationDate
					Calendar cal = Calendar.getInstance();
					cal.setTime(mediaToIngest.availableDate);
					cal.add(Calendar.DATE, vod.get(1));
					mediaToIngest.expirationDate =  cal.getTime();

					def  ingestMediaThumbnailFiles = []
					def  linkIngestMediaFiles = []
					String mediaFileID = mediaToIngest.guid;
					IngestMediaFile[] imfArr = mediaToIngest.content

					List<IngestMediaFile> mediaFileList = new ArrayList<IngestMediaFile>();
					mediaFileList.addAll(mediaToIngest.content)




					TransferInfo transferInfo = new TransferInfo()
					IngestOptions ingestOptions = new IngestOptions()

					for (IngestMediaFile imf : mediaFileList) {

						IngestMediaFile fileToDelete = new IngestMediaFile()
						fileToDelete.guid = imf.guid;
						fileToDelete.deleted = true
						linkIngestMediaFiles << fileToDelete

						IngestMediaFile mediaFileSource = new IngestMediaFile()


						mediaFileSource.url = imf.url+"&end="+mediaToIngest.expirationDate.getTime()/1000;
						mediaFileSource.assetTypes = imf.assetTypes;
						mediaFileSource.contentType = ContentType.video
						Format format1 = formatClient.getByExtension("m3u")
						mediaFileSource.format = format1
						mediaFileSource.guid = imf.guid
						mediaFileSource.deleted = false

						transferInfo.supportsStreaming = true
						mediaFileSource.transferInfo = transferInfo
						ingestOptions.method = IngestMethod.Link
						mediaFileSource.ingestOptions = ingestOptions
						mediaFileSource.isDefault = true
						linkIngestMediaFiles << mediaFileSource
					}

					mediaToIngest.setContent(linkIngestMediaFiles.toArray(new IngestMediaFile[linkIngestMediaFiles.size()]))

				}else{
					createVOD(mediaToIngest,vod);

				}


				setWorkflowOption(mediaToIngest);


				//				WorkflowOption wkflwOpt = new WorkflowOption()
				//
				//				// Set the options for the publish action
				//				wkflwOpt.setService("sharing")
				//				wkflwOpt.setMethod("auto-detect")
				//
				//				// The profile argument tells the publish service
				//				// which publish profile to use with this media.
				//
				//
				//				Map<String, String> argMap = new HashMap<String, String>()
				//				argMap.put("profileId", "http://data.share.theplatform.com/sharing/data/OutletProfile/19938")
				//
				//				wkflwOpt.setArguments(argMap)
				//
				//				// Assign the workflow request to the media.
				//				def woArr = new WorkflowOption[1]
				//				woArr[0] = wkflwOpt
				//				mediaToIngest.setWorkflowOptions(woArr);

				AdapterResultItem results = new AdapterResultItem()
				results.setMedia(mediaToIngest);
				resultQueue.put(results);

				continue;

			}

			AdapterResultItem results = new AdapterResultItem()
			results.setMedia(me.getValue());
			resultQueue.put(results);
		}
	}

	int getSeconds(String time){
		String[] timeComponents = time.split(":");
		String hour = timeComponents[0];
		String minute = timeComponents[1];
		String seconds = timeComponents[2];

		int hourIntValue = Integer.parseInt(hour);
		int minuteIntValue = Integer.parseInt(minute);
		int secondIntValue = Integer.parseInt(seconds);

		return hourIntValue*60*60+minuteIntValue*60+secondIntValue;
	}



	public String getDateTime(String startDate,String startTime, String endTime){

		SimpleDateFormat df = new SimpleDateFormat("MM-dd-yy HH:mm:ss z");
		Date start = df.parse(startDate+" "+startTime+" SGT");
		Calendar cal = Calendar.getInstance();
		cal.setTime(start);
		cal.add(Calendar.SECOND, getSeconds(endTime));
		String newTime = df.format(cal.getTime());
	}

	def replaceXmlSpecialCharacters(xmlString){

		xmlString.replaceAll("&lt;","<");
		xmlString.replaceAll("&amp;","&");
		xmlString.replaceAll("&gt;",">");
		xmlString.replaceAll("&quot;","\"");
		xmlString.replaceAll("&apos;","'");

	}

	// This method loops through the showsarray to check wheather the current media is show or not
	boolean checkForShow(String title,boolean isVod){
		boolean isShow = false;

		if (title==""||title==null){
			return isShow;
		}

		for (String key : showsDictionary.keySet()) {

			if(title.toLowerCase().startsWith(key.toLowerCase())){
				isShow = true
				imageThumb  = showsDictionary.get(key).get(1);
				break
			}
			isShow = false

		}

		if(!isShow && !isVod){
			imageThumb = "http://devapps.foxsportsasia.com/img-fsp/FICLIVE.png"
		}else{
			imageThumb = "http://devapps.foxsportsasia.com/img-fsp/FICVOD.png"
		}
		return isShow
	}


	// This method loops through the VOD Dictionary to checke weather need to create vod or not

	public List<String> isVodCreationNeeded(String title,IngestMedia mediaToIngest,String account){
		
		String specialAiringCategory = mediaToIngest.getCustomValue(account,"specialAiringCategory");
		
		if(specialAiringCategory.equalsIgnoreCase("VOD")){
			return null;
		}

		//		boolean createVod = false;

		for(String key : vodDictionary.keySet()){
			if(title.toLowerCase().startsWith(key.toLowerCase())){
				return vodDictionary.get(key);
			}

		}

		return null;

	}
	
	public Boolean isVodDictionaryContainsMeida(String title){
		

		for(String key : vodDictionary.keySet()){
			if(title.toLowerCase().startsWith(key.toLowerCase())){
				return true;
			}

		}

		return false;

	}



	public String switchCategories(String categoryName){
		boolean isCategoryNeedToMap=categoriesDictionay.containsKey(categoryName);
		if(!isCategoryNeedToMap){
			return categoryName;
		}

		return categoriesDictionay.get(categoryName);
	}
	
	// Set Media Title and Competitor values
	public void setMediaTitleAndCompetitorTexts(String mediaGuid,String mediaTitle,String[] row, Exception ex, Boolean update,IngestMedia media, Media oldMedia){
		

			String competitorTexts = removeHashString(row[11].replaceAll("\"",""))

			if(row[9].replaceAll("\"", "")!=""){
				if(competitorTexts!=""||competitorTexts!=" "||competitorTexts!=null||!competitorTexts.equalsIgnoreCase("")||!competitorTexts.equalsIgnoreCase(" ")){
					media.title = row[9].replaceAll("\"", "") + " - " + competitorTexts
				}else{

					media.title = row[9].replaceAll("\"", "")
				}

				// Remove the pound sign from HK text file
				int poundIngex = media.title.indexOf("#");
				if (poundIngex != -1)
				{
					media.title = media.title.substring(0, poundIngex);
				}
			}else{
				if(!update){
					ex = new Exception ("Media item requires some kind of title.")
					return;
				}
			}
			
			// HK media have - at the end, remove the -
			if(media.title.endsWith("- ")||media.title.endsWith("-")){
				media.title = media.title.substring(0, media.title.length()-2);
			}
			// if new media set the country list, else check for the county code, if country code dosen't exist add the country code to media countries
			if (!update){
				media.countries =countryMap.get(row[0].replaceAll("\"", "")).toArray();
			} else{


				List<String> list = new ArrayList<String>();
				list.addAll(oldMedia.countries)

				for(String countryString : countryMap.get(row[0].replaceAll("\"", "")).toArray()){

					if (list.contains(countryString)){
						continue;
					} else{
						list.add(countryString)

					}
				}

				media.countries = list.toArray()

			}


			// set media custom values


			media.setCustomValue(accountID, "allowedCountry",media.countries)

			media.excludeCountries = false

	}

	// Text Line parsed for VOD media
	public void vodMediaParsed(String [] rowValues) {

		def ingestMediaThumbnailFiles = []
		def  linkIngestMediaFiles = []
		def  catList = []
		Boolean update = false
		Exception ex = null
		Media oldMedia = null

		final String[] row = rowValues;//event.getRow()
		//create new media object
		IngestMedia media = new IngestMedia()
		//create new media file object
		IngestMediaFile mediaFile = new IngestMediaFile()
		//set the file in the media object
		media.setContent(mediaFile)
		
		//Media Title
		String mediaTitle = row[9].replaceAll("\"", "")
		
		// Check for the unique id , 18th column of the text file is unique column (Title Broadcast reference)
		if(row[18]!=""){
			String mediaGuid = row[18].replaceAll("\"", "")

			if(!mediaGuid.contains("NONLL")){
				mediaGuid = mediaGuid + "NONLL";
			}
			
			// Check for the existing media, if media exist update or else create new record
			if (mediaHelper.mediaExistsByGuid(mediaGuid))
			{

				update = true
				oldMedia = mediaHelper.getMediaByGuid(mediaGuid)
				mediaTitle = oldMedia.title

			} else{
				update = false

			}

			// set refrence id for each media
			media.guid =mediaGuid
			
			//Set the Media Title and Competitor Values
			setMediaTitleAndCompetitorTexts(mediaGuid,mediaTitle,row,ex,update,media,oldMedia);
		}else{
			ex = new Exception ("Media item requires unique ID")
		}
		
		

		if(ex==null){
			
			//Set Media Custom Values
			setMediaValues(media,row,media.availableDate,media.expirationDate,checkForShow(mediaTitle,true),linkIngestMediaFiles,ingestMediaThumbnailFiles,update,true);
		}



		AdapterResultItem results = new AdapterResultItem()
		if(ex!=null){
			results.setException(ex)
			resultQueue.put(results)
		}else{


			// same show can be available in diffrent countries.
			// Add media to temporary media map
			if(!isObjectAddedToTheQue){

				isObjectAddedToTheQue = true;
				mediaMap.put(media.guid,media);

			}

			else{

				// Check wether media with same ID exist , if so append new country list to existing country list
				IngestMedia existingMedia =  mediaMap.get(media.guid);

				if(existingMedia){
					List<String> list = new ArrayList<String>();
					list.addAll(existingMedia.countries)

					for(String countryString : countryMap.get(row[0].replaceAll("\"", "")).toArray()){

						if (list.contains(countryString)){
							continue;
						} else{
							list.add(countryString)

						}
					}

					existingMedia.countries = list.toArray()
					existingMedia.setCustomValue(accountID, "allowedCountry",existingMedia.countries)



					IngestMediaFile[] imfArr = existingMedia.getContent()
					String mediaFileID = row[17].replaceAll("\"", "")+row[0].replaceAll("\"","")
					for (imf in imfArr)
					{

						linkIngestMediaFiles << imf
					}

					existingMedia.setContent(linkIngestMediaFiles.toArray(new IngestMediaFile[linkIngestMediaFiles.size()]))



				}else{

					mediaMap.put(media.guid,media);

				}


			}


		}
	}



	// Text File each line parsed for Live Media
	public void lineParsed(String [] rowValues) {

		def ingestMediaThumbnailFiles = []
		def  linkIngestMediaFiles = []
		def  catList = []
		Boolean update = false
		Exception ex = null
		Media oldMedia = null

		final String[] row = rowValues;//event.getRow()
		//create new media object
		IngestMedia media = new IngestMedia()
		//create new media file object
		IngestMediaFile mediaFile = new IngestMediaFile()
		//set the file in the media object
		media.setContent(mediaFile)

		String mediaTitle = row[9].replaceAll("\"", "")
		// Check for the unique id , 18th column of the text file is unique column (Title Broadcast reference)
		
		// Check for the unique id , 18th column of the text file is unique column (Title Broadcast reference)
		if(row[17]!=""){
			String mediaGuid = row[17].replaceAll("\"", "")+"ODL"
			
			// Check for the existing media, if media exist update or else create new record
			if (mediaHelper.mediaExistsByGuid(mediaGuid))
			{

				update = true
				oldMedia = mediaHelper.getMediaByGuid(mediaGuid)
				mediaTitle = oldMedia.title

			} else{
				update = false

			}

			// set refrence id for each media
			media.guid =mediaGuid
			
			//Set the Media Title and Competitor Values
			setMediaTitleAndCompetitorTexts(mediaGuid,mediaTitle,row,ex,update,media,oldMedia);
		}else{
			ex = new Exception ("Media item requires unique ID")
		}
		
//		if(row[17]!=""){
//			String mediaGuid = row[17].replaceAll("\"", "")+"ODL"
//
//			// Check for the existing media, if media exist update or else create new record
//			if (mediaHelper.mediaExistsByGuid(mediaGuid))
//			{
//
//				update = true
//				oldMedia = mediaHelper.getMediaByGuid(mediaGuid)
//				mediaTitle = oldMedia.title
//
//			} else{
//				update = false
//
//			}
//
//			// set refrence id for each media
//			media.guid =mediaGuid
//
//
//
//
//			String competitorTexts = removeHashString(row[11].replaceAll("\"",""))
//
//			if(row[9].replaceAll("\"", "")!=""){
//				if(competitorTexts!=""||competitorTexts!=" "||competitorTexts!=null||!competitorTexts.equalsIgnoreCase("")||!competitorTexts.equalsIgnoreCase(" ")){
//					media.title = row[9].replaceAll("\"", "") + " - " + competitorTexts
//				}else{
//
//					media.title = row[9].replaceAll("\"", "")
//				}
//
//				// Remove the pound sign from HK text file
//				int poundIngex = media.title.indexOf("#");
//				if (poundIngex != -1)
//				{
//					media.title = media.title.substring(0, poundIngex);
//				}
//			}else{
//				if(!update){
//					ex = new Exception ("Media item requires some kind of title.")
//				}
//			}
//		}else{
//			ex = new Exception ("Media item requires unique ID")
//
//		}

		if(ex==null){


//			// HK media have - at the end, remove the -
//
//			if(media.title.endsWith("- ")||media.title.endsWith("-")){
//				media.title = media.title.substring(0, media.title.length()-2);
//			}
//			// if new media set the country list, else check for the county code, if country code dosen't exist add the country code to media countries
//			if (!update){
//				media.countries =countryMap.get(row[0].replaceAll("\"", "")).toArray();
//			} else{
//
//
//				List<String> list = new ArrayList<String>();
//				list.addAll(oldMedia.countries)
//
//				for(String countryString : countryMap.get(row[0].replaceAll("\"", "")).toArray()){
//
//					if (list.contains(countryString)){
//						continue;
//					} else{
//						list.add(countryString)
//
//					}
//				}
//
//				media.countries = list.toArray()
//
//			}
//
//
//			// set media custom values
//
//
//			media.setCustomValue(accountID, "allowedCountry",media.countries)
//
//			media.excludeCountries = false


			// set available and expiration date for each media
			SimpleDateFormat pdf = new SimpleDateFormat("MM-dd-yy HH:mm:ss z");
			media.availableDate = pdf.parse( row[4].replaceAll("\"", "") + " "+row[5].replaceAll("\"", "")+" SGT")
			media.expirationDate = pdf.parse(getDateTime(row[4].replaceAll("\"", ""),row[5].replaceAll("\"", ""),row[6].replaceAll("\"", "")))

			setMediaValues(media,row,media.availableDate,media.expirationDate,checkForShow(mediaTitle,false),linkIngestMediaFiles,ingestMediaThumbnailFiles,update,false);
		}



		AdapterResultItem results = new AdapterResultItem()
		if(ex!=null){
			results.setException(ex)
			resultQueue.put(results)
		}else{


			// same show can be available in diffrent countries.
			// Add media to temporary media map
			if(!isObjectAddedToTheQue){

				isObjectAddedToTheQue = true;
				mediaMap.put(media.guid,media);

			}

			else{

				// Check wether media with same ID exist , if so append new country list to existing country list
				IngestMedia existingMedia =  mediaMap.get(media.guid);

				if(existingMedia){
					List<String> list = new ArrayList<String>();
					list.addAll(existingMedia.countries)

					for(String countryString : countryMap.get(row[0].replaceAll("\"", "")).toArray()){

						if (list.contains(countryString)){
							continue;
						} else{
							list.add(countryString)

						}
					}

					existingMedia.countries = list.toArray()
					existingMedia.setCustomValue(accountID, "allowedCountry",existingMedia.countries)



					IngestMediaFile[] imfArr = existingMedia.getContent()
					String mediaFileID = row[17].replaceAll("\"", "")+row[0].replaceAll("\"","")
					for (imf in imfArr)
					{

						linkIngestMediaFiles << imf
					}

					existingMedia.setContent(linkIngestMediaFiles.toArray(new IngestMediaFile[linkIngestMediaFiles.size()]))



				}else{

					mediaMap.put(media.guid,media);

				}


			}


		}
	}


	public class LineParsedEvent
	{

		//row values (String[] of each column)
		private String[] rowValues

		public LineParsedEvent(final String[] rowValues)
		{

			this.rowValues = rowValues
		}

		/**
		 * @return row values (String[] of each column)
		 */
		public String[] getRow()
		{
			return this.rowValues
		}
	}

	public class TextParser
	{
		LineParsedListener listener = null;
		InputStream inputStream = null;

		public TextParser(InputStream xlsInputStream, LineParsedListener listener)
		{
			this.listener = listener;
			this.inputStream = xlsInputStream;
		}

		/**
		 * Process each line
		 */
		public void process(Dictionary channelDictionay)
		{


			InputStreamReader is = null ;
			BufferedReader br = null;
			String line = "";
			String cvsSplitBy = ",";
			int rowIndex = 0;

			try {

				is =  new InputStreamReader(inputStream);
				br =  new BufferedReader(is);
				while ((line = br.readLine()) != null) {



					String[] dataValue = line.split("\t");
					String channelName = dataValue[0];
					String value2 = dataValue[1];
					String progrmName = dataValue[9];
					String airCategory = dataValue[13];
					String vodRights = dataValue[19];
					String vodReferenceID = dataValue[18];
					
					// Extract Media which have VOD Rights Flag is set to Y (YES) 
					if( channelDictionay.get(channelName)!=null && (vodRights == "Y") && vodReferenceID != null && vodReferenceID.trim() != "" && !isVodDictionaryContainsMeida(dataValue[9].replaceAll("\"", ""))){
						
						listener.vodMediaParsed(dataValue);
						
					}

					// Only Extract record for each channel code in channelDictionay & airing categoy is either Live (L) or Simulcast (S)
					if( channelDictionay.get(channelName)!=null && (airCategory == "L")||progrmName.startsWith("FOX Sports FC")){

						listener.lineParsed(dataValue);

					}


				}
				// text file processing is finished
				listener.finishedProcessing()



			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}


		}
	}

	/**
	 * Interface to implement once row is parsed
	 */
	public interface LineParsedListener extends EventListener
	{
		public void vodMediaParsed(String[] rowValues)
		public void lineParsed(String [] rowValues)
		public void finishedProcessing()

	}

}